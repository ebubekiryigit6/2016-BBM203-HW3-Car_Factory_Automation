Department Frame has been created.
Department Paint has been created.
Department Engine has been created.
Department Electronics has been created.
Department Indoor has been created.
Department Test has been created.
- Frame1 Frame2
               - Paint1 Paint2
                              - Engine1 Engine2 Engine3 Engine4
                                                               - Electronics1 Electronics2
                                                                                          - Indoor1 Indoor2 Indoor3
                                                                                                                   - Test1 Test2

Command: Report Car 9 G322
----------------------
|Report for Golf G322|
----------------------
Frame:2, Paint:0, Engine:0, Electronics:0, Indoor:0, Test:0, | Start Time7 | Complete:12.5% | Not complete

Command: Report Cars 19
----------------------
|Report for Golf G234|
----------------------
Frame:2, Paint:4, Engine:3, Electronics:3, Indoor:2, Test:2, | Start Time1 | Complete:100% | Complete
------------------------
|Report for Touran T174|
------------------------
Frame:2, Paint:4, Engine:3, Electronics:3, Indoor:2, Test:2, | Start Time2 | Complete:100% | Complete
-----------------------
|Report for Jetta J041|
-----------------------
Frame:2, Paint:4, Engine:3, Electronics:3, Indoor:2, Test:1, | Start Time3 | Complete:93.75% | Not complete
------------------------
|Report for Passat P862|
------------------------
Frame:2, Paint:4, Engine:3, Electronics:3, Indoor:2, Test:0, | Start Time4 | Complete:87.5% | Not complete
----------------------
|Report for Golf G322|
----------------------
Frame:2, Paint:4, Engine:3, Electronics:2, Indoor:0, Test:0, | Start Time7 | Complete:68.75% | Not complete
-----------------------
|Report for Jetta J051|
-----------------------
Frame:2, Paint:4, Engine:3, Electronics:1, Indoor:0, Test:0, | Start Time8 | Complete:62.5% | Not complete
------------------------
|Report for Passat P455|
------------------------
Frame:2, Paint:4, Engine:1, Electronics:0, Indoor:0, Test:0, | Start Time11 | Complete:43.75% | Not complete
----------------------
|Report for Golf G922|
----------------------
Frame:2, Paint:4, Engine:0, Electronics:0, Indoor:0, Test:0, | Start Time12 | Complete:37.5% | Not complete
--------------------------
|Report for Scirroco S323|
--------------------------
Frame:2, Paint:1, Engine:0, Electronics:0, Indoor:0, Test:0, | Start Time15 | Complete:18.75% | Not complete
------------------------
|Report for Touran T876|
------------------------
Frame:2, Paint:0, Engine:0, Electronics:0, Indoor:0, Test:0, | Start Time16 | Complete:12.5% | Not complete
-----------------------
|Report for Jetta J731|
-----------------------
Frame:1, Paint:0, Engine:0, Electronics:0, Indoor:0, Test:0, | Start Time19 | Complete:6.25% | Not complete

Command: Report Departments 19
--------------------------------
|Report for Department "Test 1"|
--------------------------------
I am currently processing Jetta J041
--------------------------------
|Report for Department "Test 2"|
--------------------------------
Test 2 is now free.
----------------------------------
|Report for Department "Indoor 1"|
----------------------------------
Indoor 1 is now free.
Processed Cars
1. Golf G234
2. Jetta J041
----------------------------------
|Report for Department "Indoor 2"|
----------------------------------
I am currently processing Passat P862
Processed Cars
1. Touran T174
----------------------------------
|Report for Department "Indoor 3"|
----------------------------------
Indoor 3 is now free.
---------------------------------------
|Report for Department "Electronics 1"|
---------------------------------------
I am currently processing Golf G322
Processed Cars
1. Golf G234
2. Jetta J041
---------------------------------------
|Report for Department "Electronics 2"|
---------------------------------------
I am currently processing Jetta J051
Processed Cars
1. Touran T174
2. Passat P862
----------------------------------
|Report for Department "Engine 1"|
----------------------------------
I am currently processing Passat P455
Processed Cars
1. Golf G234
2. Jetta J041
3. Golf G322
----------------------------------
|Report for Department "Engine 2"|
----------------------------------
Engine 2 is now free.
Processed Cars
1. Touran T174
2. Passat P862
3. Jetta J051
----------------------------------
|Report for Department "Engine 3"|
----------------------------------
Engine 3 is now free.
----------------------------------
|Report for Department "Engine 4"|
----------------------------------
Engine 4 is now free.
---------------------------------
|Report for Department "Paint 1"|
---------------------------------
I am currently processing Scirroco S323
Processed Cars
1. Golf G234
2. Jetta J041
3. Golf G322
4. Passat P455
---------------------------------
|Report for Department "Paint 2"|
---------------------------------
I am currently processing Golf G922
Processed Cars
1. Touran T174
2. Passat P862
3. Jetta J051
---------------------------------
|Report for Department "Frame 1"|
---------------------------------
I am currently processing Jetta J731
Processed Cars
1. Golf G234
2. Jetta J041
3. Golf G322
4. Passat P455
5. Scirroco S323
---------------------------------
|Report for Department "Frame 2"|
---------------------------------
I am currently processing Touran T876
Processed Cars
1. Touran T174
2. Passat P862
3. Jetta J051
4. Golf G922