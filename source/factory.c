/**
 *
 * Author:      Ebubekir Yigit
 *
 * ID:          21328629
 *
 * File:        factory.c
 *
 * Purpose:     Car Production Line Simulation
 *
 *              According to the input, creates departments and improve the cars in the departments.
 *              When the cars enter the departments, the entrance time of the cars and the transition time from the departments are kept.
 *              According to the report command, the status of departing departments and cars is printed.
 *
 *
 * Compile:     gcc factory.c -o factory -ansi -pedantic-errors -Wall
 *
 * Run:         ./factory <input-file>
 *
 *              files' extension is .txt
 *
 *
 * Input:       none
 *
 * Output:
 *              Prints created Departments names.
 *              Prints created all departments.
 *              Prints departments status at given time.
 *              Prints Car/Cars status at given time.
 *
 *
 * Notes:
 *              Please enter the inputs according to the format.
 *
 *
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef struct {
    char** array;
    int front;
    int rear;
    int size;
}Queue;

/* department that car entered */
typedef struct {
    int beginTime;
    int endTime;
    char departmentName[30];
    int departmentTime;
}CarInDepartment;

/* departments stack based array that car entered */
typedef struct {
    CarInDepartment **carInDepartment;
    int size;
    int top;
}CarInDepartmentsArray;

/* keep car info */
typedef struct {
    char carModel[30];
    char carCode[10];
    int startTime;
    int inProcess;
    CarInDepartmentsArray carInDepartmentsArray;
}Car;

/* stack based cars array */
typedef struct {
    Car **cars;
    int size;
    int top;
}ProduceCommandsArray;

/* stack based departments array */
typedef struct {
    struct Department **departments;
    int size;
    int top;
}AddDeptCommandsArray;

/* Car that department proceeds */
typedef struct {
    int beginTime;
    int endTime;
    char carCode[10];
    char carModel[30];
}BuildedCars;

/* Stack based cars array that department proceeds */
typedef struct {
    BuildedCars **buildedCars;
    int top;
    int size;
}BuildedCarsArray;

/* keeps department info */
struct Department{
    struct Department *next;
    struct Department *prev;
    struct Department *child;
    struct Department *parent;
    char deptType[30];
    char name[30];
    int processTime;
    char workingCarCode[10];
    int carEnterTime;
    int isEmpty;
    ProduceCommandsArray buildingCar;
    BuildedCarsArray buildedCarsArray;
};


#define ADD_DEPT "AddDept"
#define PRINT_FACTORY "PrintFactory"
#define PRINT_FACTORY_NL "PrintFactory\n"
#define PRINT_FACTORY_DEV "PrintFactory\r\n"
#define PRODUCE "Produce"
#define REPORT "Report"
#define NEWLINE "\r\n"


void initializeDepartmentsArray(AddDeptCommandsArray *addDeptCommandsArray, int initialSize);
void initializeCarsArray(ProduceCommandsArray *produceCommandsArray, int initialSize);
struct Department* getNewDepartment();
void printFactory(struct Department *headPointer);
void addDepartmentArray(AddDeptCommandsArray *addDeptCommandsArray, struct Department *department);
void addCarsArray(ProduceCommandsArray *produceCommandsArray, Car *newCar);
void reportCommand(Queue *reportCommandArray, char* singleLine);
void initializeQueue(Queue *queue, int initialSize) ;
void enqueue(Queue *queue, char* element);
void prevControl(struct Department *childControlPtr, int GLOBAL_TIME, struct Department *headPointer, ProduceCommandsArray *allCars);
void initializeCarInDepartmentsArray(CarInDepartmentsArray *carInDepartmentsArray, int initialSize);
void initializeBuildedCarsArray(BuildedCarsArray *buildedCarsArray, int initialSize);
void addBuildedCarsArray(BuildedCarsArray *buildedCarsArray, BuildedCars *newBuildedCar);
void addCarInDeptArray(CarInDepartmentsArray *carInDepartmentsArray, CarInDepartment *carInDepartment);
void saveDeptsAndCars(struct Department *newPointer, int GLOBAL_TIME);
void tryAddingCarFirstDept(ProduceCommandsArray *allCars, struct Department *headPointer, int GLOBAL_TIME);
char* findCarPriority(struct Department *prevPointer, int GLOBAL_TIME);
void reportAllCommands(Queue *reportCommandArray, struct Department *headPointer, ProduceCommandsArray *allCars);
void printNTimesThis(char* value, int N);
void oneCarReport(char* reportLine, ProduceCommandsArray *finishedCars,struct Department *headPointer);
void getOneCarReport(Car *car, int TIME,struct Department *headPointer);
double getDepartmentTotalTime(struct Department *headPointer);
void allCarsReport(char *reportLine, ProduceCommandsArray *finishedCars,struct Department *headPointer);
int isEmptyQueue(Queue *queue);
void addDepartment(char* singleLine, AddDeptCommandsArray *allDepartments, struct Department *headPointer);
void allDepartmentsReport(char *reportLine, struct Department *headPointer);
void controlDepartmentStatus(ProduceCommandsArray *allCars, struct Department *headPointer, int GLOBAL_TIME, ProduceCommandsArray *finishedCars);
void buildAllCars(ProduceCommandsArray *allCars, struct Department *headPointer, int GLOBAL_TIME,ProduceCommandsArray *finishedCars);
void produceCars(char* singleLine, ProduceCommandsArray *allCars);
void lineParser(char* singleLine, AddDeptCommandsArray *allDepartments, ProduceCommandsArray *allCars, struct Department *headPointer, Queue *reportCommandArray);
void readFile(char *inputFileName, AddDeptCommandsArray *allDepartments, ProduceCommandsArray *allCars, struct Department *headPointer, Queue *reportCommandArray);
void reportOneDepartment(struct Department *pointer, int TIME, int cnt);


/*************************************   STARTS MAIN ***************************************/
int main(int argc, char* argv[]) {

    struct Department *headPointer;

    AddDeptCommandsArray allDepartments; /* all created departments */
    ProduceCommandsArray allCars; /* all given cars */
    ProduceCommandsArray finishedCars; /* all processed cars */
    Queue reportCommandArray; /* keeps report commands */


    int GLOBAL_TIME;  /* Factory time */

    headPointer = (struct Department*)malloc(sizeof(struct Department));
    headPointer->next = NULL;
    headPointer->prev = NULL;

    GLOBAL_TIME = 0;

    /* Stack or Queue based arrays initialized. */
    initializeDepartmentsArray(&allDepartments,10);
    initializeCarsArray(&allCars,10);
    initializeCarsArray(&finishedCars,10);
    initializeQueue(&reportCommandArray,10);


    /* reads file and parse all commands
                  keeps all information in arrays */
    readFile(argv[1],&allDepartments, &allCars,headPointer,&reportCommandArray);

    /*
     *  produce all cars
     */
    buildAllCars(&allCars,headPointer,GLOBAL_TIME,&finishedCars);

    /*
     *  reports all cars and departments at given time
     */
    reportAllCommands(&reportCommandArray,headPointer,&finishedCars);

    free(headPointer);
    /*
    free(allDepartments.departments);
    free(allCars.cars);
    free(finishedCars.cars);
    free(reportCommandArray.array);
    */
    return 0;
}
/******************************************** END OF MAIN ****************************************************/


/*******************************************    read and parsing line *******************************************************/

void readFile(char *inputFileName, AddDeptCommandsArray *allDepartments, ProduceCommandsArray *allCars, struct Department *headPointer, Queue *reportCommandArray){

    FILE *file;
    char line [BUFSIZ];

    file = fopen (inputFileName, "r");
    if ( file != NULL ) {
        while (fgets (line, sizeof line, file) != NULL )
        {
            /*  sends a line to lineParser() */
            lineParser(line,allDepartments,allCars,headPointer,reportCommandArray);
        }
        fclose (file);
    }
    else {
        perror (inputFileName);
    }

}

void lineParser(char* singleLine, AddDeptCommandsArray *allDepartments, ProduceCommandsArray *allCars, struct Department *headPointer, Queue *reportCommandArray){

    char* token;
    char temp[BUFSIZ];
    strcpy(temp,singleLine);

    token = strtok(temp," ");

    if (strcmp(token,PRINT_FACTORY) == 0 || strcmp(token,PRINT_FACTORY_NL) == 0 || strcmp(token,PRINT_FACTORY_DEV) == 0){
        /* prints all created departments*/
        printFactory(headPointer);
    }
    else if (strcmp(token,PRODUCE) == 0) {
        /* keeps cars info in allCars array */
        produceCars(singleLine,allCars);
    }

    else if (strcmp(token,ADD_DEPT) == 0) {
        /* creates a department and its childs, adds them last pointer */
        addDepartment(singleLine,allDepartments,headPointer);
    }

    else if (strcmp(token,REPORT) == 0){
        /* keeps report commands in array*/
        reportCommand(reportCommandArray,singleLine);
    }


}


/******************************************     adding new departments and its childs      ************************************************/

/****************************************************************************************************************
 * creates new Department and its childs and adds last linked pointer
 *
 * @param singleLine addDept command line
 * @param allDepartments departments stack based array
 * @param headPointer factory headPointer
 */
void addDepartment(char* singleLine, AddDeptCommandsArray *allDepartments, struct Department *headPointer){

    int numberOfDept;
    char* deptType;
    char* token;
    char temp[BUFSIZ];
    int time;
    char* deptName;
    char *newDepName;
    char *depCount;
    int i;


    struct Department *newDepartment;
    struct Department *pointer;
    struct Department *newDep;
    struct Department *ptr;

    strcpy(temp,singleLine);

    token = strtok(temp,NEWLINE);
    token = strtok(temp," ");
    token = strtok(NULL," ");

    numberOfDept = atoi(token);

    token = strtok(NULL," ");
    deptType = (char*)malloc(30 * sizeof(char));
    deptType[0] = '\0';
    strcpy(deptType,token);
    token = strtok(NULL," ");
    time = atoi(token);

    deptName = (char*)malloc(30 * sizeof(char));
    deptName[0] = '\0';
    strcat(deptName,deptType);
    strcat(deptName,"1\0");

    printf("Department %s has been created.\n",deptType);

    newDepartment = getNewDepartment();
    strcpy(newDepartment->deptType,deptType);
    newDepartment->processTime = time;
    strcpy(newDepartment->name,deptName);


    /* add main department in departments array*/
    addDepartmentArray(allDepartments,newDepartment);


    /*  first department adding */
    if (headPointer->next == NULL){
        headPointer->next = newDepartment;
        newDepartment->prev = NULL;
        newDepartment->next = NULL;
    }
        /* some departments found, go to last department's next */
    else {
        pointer = headPointer;
        while (pointer->next != NULL){
            pointer = pointer->next;
        }
        pointer->next = newDepartment;
        newDepartment->prev = pointer;
        newDepartment->next = NULL;
    }

    /* create child departments and add them main department's botton */
    for (i = 0; i < numberOfDept - 1; i++){
        newDep = getNewDepartment();
        strcpy(newDep->deptType,deptType);
        newDep->processTime = time;

        newDepName = (char*)malloc(30 * sizeof(char));
        depCount = (char*)malloc(10 * sizeof(char));
        newDepName[0] = '\0';
        depCount[0] = '\0';
        snprintf(depCount, sizeof(depCount),"%d",i+2);

        /*         itoa(i+2,depCount,10);      not used in dev */

        strcat(newDepName,deptType);
        strcat(newDepName,depCount);
        strcpy(newDep->name,newDepName);

        /* adding child department */
        addDepartmentArray(allDepartments,newDep);


        if (newDepartment->child == NULL){
            newDepartment->child = newDep;
            newDep->parent = newDepartment;
            newDep->child = NULL;
            newDep->prev = newDepartment->prev;
        }
        else {
            ptr = newDepartment;
            while (ptr->child != NULL){
                ptr = ptr->child;
            }
            ptr->child = newDep;
            newDep->parent = ptr;
            newDep->child = NULL;
            newDep->prev = ptr->prev;
        }
    }

}

/***********************************    prints all created departments      ***************************************/

/****************************************************************************************************************
 * prints all created departments
 *
 * @param headPointer
 */
void printFactory(struct Department *headPointer){
    struct Department *point;
    int counter;
    int spaceCounter;
    int j;

    point = headPointer;

    if (point->next == NULL){
        printf("No department created ! \n");
    }
    else {
        counter = 0;  /* string len counter */
        spaceCounter = 0;  /* space counter */
        while (point->next != NULL){
            for (j = 0; j < counter + spaceCounter; j++){
                printf(" ");
            }
            printf("- %s ",point->next->name);
            counter += strlen(point->next->name);
            spaceCounter+=2;
            point = point->next;
            while (point->child != NULL){
                spaceCounter++;
                printf("%s ",point->child->name);
                counter += strlen(point->child->name);
                point = point->child;
            }
            printf("\n");
            while (point->parent != NULL){
                point = point->parent;
            }
        }
    }
    printf("\n");
}


/*************************************      initialize and keep all cars       ********************************************/

/****************************************************************************************************************
 * keeps all cars info in allCars array
 *
 * @param singleLine produce command line
 * @param allCars all cars in given input commands
 * @param headPointer
 * @param GLOBAL_TIME factory time
 */
void produceCars(char* singleLine, ProduceCommandsArray *allCars){
    int startTime;
    char carCode[10];
    char carModel[30];
    char* token;
    char temp[BUFSIZ];

    Car *car;

    strcpy(temp,singleLine);
    token = strtok(temp,NEWLINE);
    token = strtok(temp," ");
    token = strtok(NULL," ");

    startTime = atoi(token);

    token = strtok(NULL," ");
    strcpy(carModel,token);

    token = strtok(NULL," ");
    strcpy(carCode,token);

    /* initialize a car and add all cars array */
    car = (Car*)malloc(sizeof(Car));
    strcpy(car->carCode,carCode);
    strcpy(car->carModel,carModel);
    car->startTime = startTime;
    car->inProcess = 0;
    initializeCarInDepartmentsArray(&car->carInDepartmentsArray,10);

    addCarsArray(allCars,car);


}

/************************************      prints all departments and car/cars reports given time       **************************************/

/****************************************************************************************************************
 * keeps all report commands
 *
 * @param reportCommandArray keeps report commands
 * @param singleLine
 */
void reportCommand(Queue *reportCommandArray, char* singleLine){

    /* keep the report command */

    char* token;
    char temp[BUFSIZ];
    char *reportLine;

    strcpy(temp,singleLine);
    token = strtok(temp,NEWLINE);

    reportLine = (char*)malloc(BUFSIZ * sizeof(char));
    reportLine[0] = '\0';
    strcpy(reportLine,temp);

    enqueue(reportCommandArray,reportLine);


}





/************************************************     begining produce cars   *******************************************************/


/****************************************************************************************************************
 * works until all cars finished, cars proceed in departments in order
 *
 * @param allCars
 * @param headPointer
 * @param GLOBAL_TIME
 * @param finishedCars
 */
void buildAllCars(ProduceCommandsArray *allCars, struct Department *headPointer, int GLOBAL_TIME,ProduceCommandsArray *finishedCars){

    while (allCars->top != finishedCars->top) {

        GLOBAL_TIME++;

        tryAddingCarFirstDept(allCars, headPointer, GLOBAL_TIME);

        controlDepartmentStatus(allCars, headPointer, GLOBAL_TIME, finishedCars);
    }
}

/****************************************************************************************************************
 * if last department's work finished, add cars in finishedCars
 * Other departments checks previous department's work is finished
 *
 * @param allCars
 * @param headPointer
 * @param GLOBAL_TIME
 * @param finishedCars
 */
void controlDepartmentStatus(ProduceCommandsArray *allCars, struct Department *headPointer, int GLOBAL_TIME, ProduceCommandsArray *finishedCars){
    struct Department *pointer;
    struct Department *lastDepartment;
    struct Department *controlPtr;
    struct Department *childControlPtr;

    pointer = headPointer;

    if (pointer->next != NULL){
        pointer = pointer->next;
        while (pointer->next != NULL){
            pointer = pointer->next;
        }

        lastDepartment = pointer;
        while (lastDepartment != NULL){
            if (lastDepartment->next == NULL){
                if (lastDepartment->isEmpty == 0){
                    if (lastDepartment->carEnterTime + lastDepartment->processTime <= GLOBAL_TIME){
                        addCarsArray(finishedCars,lastDepartment->buildingCar.cars[lastDepartment->buildingCar.top]);
                        lastDepartment->buildingCar.top = -1;
                        lastDepartment->isEmpty = 1;
                        lastDepartment->workingCarCode[0] = '\0';
                        /*
                        printf("Time: %d  ---  Car: %s  --  CarCode: %s    FINISHED COMPLETELY \n",GLOBAL_TIME,finishedCars->cars[finishedCars->top]->carModel,finishedCars->cars[finishedCars->top]->carCode);
                         */
                    }
                }
            }
            lastDepartment = lastDepartment->child;
        }


        controlPtr = pointer;
        while (controlPtr->prev != NULL){
            childControlPtr = controlPtr;
            while (childControlPtr != NULL){
                if (childControlPtr->isEmpty == 1){
                    prevControl(childControlPtr,GLOBAL_TIME,headPointer,allCars);
                }
                childControlPtr = childControlPtr->child;
            }
            controlPtr = controlPtr->prev;
        }
    }
}

/****************************************************************************************************************
 * Previous department's works is finished?
 * Has Car at previous department higher priority?
 *
 * @param childControlPtr
 * @param GLOBAL_TIME
 * @param headPointer
 * @param allCars
 */
void prevControl(struct Department *childControlPtr, int GLOBAL_TIME, struct Department *headPointer, ProduceCommandsArray *allCars){

    struct Department *prevPointer;

    prevPointer = childControlPtr->prev;

    while (prevPointer != NULL){
        if (prevPointer->isEmpty == 0){
            if (prevPointer->carEnterTime + prevPointer->processTime <= GLOBAL_TIME) {

                /* if two departments finish their work same time, which car have small time */
                if (strcmp(findCarPriority(prevPointer, GLOBAL_TIME), prevPointer->name) == 0) {

                    if (childControlPtr->isEmpty == 1) {
                        /*
                        printf("Time: %d --  %s 's works is finished on %s \n", GLOBAL_TIME, prevPointer->name,prevPointer->buildingCar.cars[prevPointer->buildingCar.top]->carCode);
                         */
                        addCarsArray(&childControlPtr->buildingCar, prevPointer->buildingCar.cars[prevPointer->buildingCar.top]);
                        prevPointer->buildingCar.top = -1;
                        prevPointer->isEmpty = 1;
                        prevPointer->workingCarCode[0] = '\0';

                        /*
                        printf("Time: %d --   .%s.  %s 'in isini devraldi -- %s\n ", GLOBAL_TIME, chidControlPtr->name, prevPointer->name, chidControlPtr->buildingCar.cars[chidControlPtr->buildingCar.top]->carCode);
                         */
                        childControlPtr->isEmpty = 0;
                        childControlPtr->carEnterTime = GLOBAL_TIME;
                        strcpy(childControlPtr->workingCarCode,
                               childControlPtr->buildingCar.cars[childControlPtr->buildingCar.top]->carCode);

                        saveDeptsAndCars(childControlPtr, GLOBAL_TIME);
                    }
                }
            }
        }
        prevPointer = prevPointer->child;
    }

    tryAddingCarFirstDept(allCars,headPointer,GLOBAL_TIME);
}

/****************************************************************************************************************
 * gives department name that has higher priority
 *
 * @param prevPointer
 * @param GLOBAL_TIME
 * @return
 */
char* findCarPriority(struct Department *prevPointer, int GLOBAL_TIME){
    struct Department *tempPrevPoint;
    char* deptName;
    int startTime;

    startTime = -1;
    deptName = (char*)malloc(30 * sizeof(char));
    deptName[0] = '\0';

    tempPrevPoint = prevPointer;

    while (tempPrevPoint != NULL){
        if (tempPrevPoint->isEmpty == 0){
            if (tempPrevPoint->carEnterTime + tempPrevPoint->processTime <= GLOBAL_TIME) {
                if (startTime == -1){
                    startTime = tempPrevPoint->buildingCar.cars[tempPrevPoint->buildingCar.top]->startTime;
                    strcpy(deptName,tempPrevPoint->name);
                }
                else {
                    if (tempPrevPoint->buildingCar.cars[tempPrevPoint->buildingCar.top]->startTime < startTime){
                        startTime = tempPrevPoint->buildingCar.cars[tempPrevPoint->buildingCar.top]->startTime;
                        deptName[0] = '\0';
                        strcpy(deptName,tempPrevPoint->name);
                    }
                }
            }
        }
        tempPrevPoint = tempPrevPoint->child;
    }
    return deptName;
}

/****************************************************************************************************************
 * saves all the cars entering the departments and enter times
 * saves all the departments produced cars and enter times
 *
 * @param newPointer
 * @param GLOBAL_TIME
 */
void saveDeptsAndCars(struct Department *newPointer, int GLOBAL_TIME){

    CarInDepartment *carInDepartment;
    BuildedCars *buildedCars;
    /* save car entry */
    buildedCars = (BuildedCars*)malloc(sizeof(BuildedCars));
    buildedCars->beginTime = GLOBAL_TIME;
    strcpy(buildedCars->carCode,newPointer->buildingCar.cars[newPointer->buildingCar.top]->carCode);
    strcpy(buildedCars->carModel,newPointer->buildingCar.cars[newPointer->buildingCar.top]->carModel);
    addBuildedCarsArray(&newPointer->buildedCarsArray,buildedCars);
    /* end */


    /* save department entry */
    carInDepartment = (CarInDepartment*)malloc(sizeof(CarInDepartment));
    carInDepartment->beginTime = GLOBAL_TIME;
    carInDepartment->departmentTime = newPointer->processTime;
    strcpy(carInDepartment->departmentName,newPointer->deptType);
    addCarInDeptArray(&newPointer->buildingCar.cars[newPointer->buildingCar.top]->carInDepartmentsArray,carInDepartment);
    /*  end  */
}

/****************************************************************************************************************
 * Tries to put a car in the first department.
 *
 * @param allCars
 * @param headPointer
 * @param GLOBAL_TIME
 */
void tryAddingCarFirstDept(ProduceCommandsArray *allCars, struct Department *headPointer, int GLOBAL_TIME){
    struct Department *pointer;
    int i;


    pointer = headPointer;

    for (i = 0; i <= allCars->top; i++) {
        if (allCars->cars[i]->startTime <= GLOBAL_TIME && allCars->cars[i]->inProcess == 0) {
            struct Department *newPointer = pointer->next;
            while (newPointer != NULL) {
                if (newPointer->isEmpty == 1) {
                    newPointer->isEmpty = 0;
                    newPointer->carEnterTime = GLOBAL_TIME;
                    strcpy(newPointer->workingCarCode, allCars->cars[i]->carCode);
                    allCars->cars[i]->inProcess = 1;
                    addCarsArray(&newPointer->buildingCar, allCars->cars[i]);

                    saveDeptsAndCars(newPointer,GLOBAL_TIME);
                    /*
                    printf("Time: %d   ---  Car: %s  --   CarCode: %s   is in - %s -\n",GLOBAL_TIME,allCars->cars[i]->carModel,allCars->cars[i]->carCode,newPointer->name);
                     */
                    break;
                }
                newPointer = newPointer->child;
            }
        }
    }
}

/************************************************     end of produce cars   *******************************************************/







/************************************************     begining reports  *******************************************************/

/**
 * parses report commands and send them functions
 *
 * @param reportCommandArray
 * @param headPointer
 * @param finishedCars
 */
void reportAllCommands(Queue *reportCommandArray, struct Department *headPointer, ProduceCommandsArray *finishedCars){
    int i;
    char* reportLine;
    char *token;

    if (reportCommandArray->front == -1 || reportCommandArray->front > reportCommandArray->rear){
        printf("No report commands found!\n");
    }
    else {
        for (i = reportCommandArray->front; i <= reportCommandArray->rear; i++){
            reportLine = (char*)malloc(50 * sizeof(char));
            reportLine[0] = '\0';
            strcpy(reportLine,reportCommandArray->array[i]);

            token = strtok(reportLine," ");
            token = strtok(NULL," ");
            token = strtok(NULL," ");
            if (strcmp(token,"Departments") == 0){
                allDepartmentsReport(reportCommandArray->array[i],headPointer);
            }
            else if (strcmp(token,"Cars") == 0){
                allCarsReport(reportCommandArray->array[i],finishedCars,headPointer);
            }
            else if (strcmp(token,"Car") == 0){
                oneCarReport(reportCommandArray->array[i],finishedCars,headPointer);
            }
        }
    }
}

/****************************************************************************************************************
 * traverse departments linked list and reports all departments
 *
 * @param reportLine
 * @param headPointer
 */
void allDepartmentsReport(char *reportLine, struct Department *headPointer){
    char tempLine[50];
    int TIME;
    char *token;
    struct Department *pointer;
    struct Department * childPointer;
    int cnt;

    strcpy(tempLine,reportLine);
    token = strtok(tempLine," ");
    token = strtok(NULL, " ");
    TIME = atoi(token) + 1;

    printf("Command: Report Departments %d\n",TIME - 1);

    pointer = headPointer;
    while (pointer->next != NULL){
        pointer = pointer->next;
    }
    while (pointer != NULL){
        childPointer = pointer;
        cnt = 1;
        while (childPointer != NULL){
            reportOneDepartment(childPointer,TIME,cnt);
            childPointer = childPointer->child;
            cnt++;
        }
        pointer = pointer->prev;
    }
}

/****************************************************************************************************************
 * one department report
 *
 * @param pointer
 * @param TIME
 */
void reportOneDepartment(struct Department *pointer, int TIME, int cnt){
    char *dept;
    int isEmpty;
    int i,j,k;
    char *code;
    Queue processedCars;
    char *deptCnt;

    deptCnt = (char*)malloc(10 * sizeof(char));
    dept = (char*)malloc(50 * sizeof(char));
    deptCnt[0] = '\0';
    dept[0] = '\0';
    snprintf(deptCnt, sizeof(deptCnt),"%d",cnt);
    strcat(dept,"|Report for Department \"\0");
    strcat(dept,pointer->deptType);
    strcat(dept," \0");
    strcat(dept,deptCnt);
    strcat(dept,"\"|\0");

    printNTimesThis("-",strlen(dept));
    printf("%s\n",dept);
    printNTimesThis("-",strlen(dept));

    initializeQueue(&processedCars,10);

    isEmpty = 1;
    for (i = 0; i <= pointer->buildedCarsArray.top; i++){
        if (TIME >= pointer->buildedCarsArray.buildedCars[i]->beginTime && TIME < pointer->buildedCarsArray.buildedCars[i]->beginTime + pointer->processTime){
            isEmpty = 0;
            printf("I am currently processing %s %s\n",pointer->buildedCarsArray.buildedCars[i]->carModel,pointer->buildedCarsArray.buildedCars[i]->carCode);
        }
        else if (pointer->buildedCarsArray.buildedCars[i]->beginTime + pointer->processTime < TIME){
            code = (char*)malloc(10 * sizeof(char));
            code[0] = '\0';
            strcat(code,pointer->buildedCarsArray.buildedCars[i]->carModel);
            strcat(code," \0");
            strcat(code,pointer->buildedCarsArray.buildedCars[i]->carCode);
            enqueue(&processedCars,code);
        }
    }
    if (isEmpty){
        printf("%s is now free.\n",pointer->name);
    }
    if (!isEmptyQueue(&processedCars)){
        k = 1;
        printf("Processed Cars\n");
        for (j = processedCars.front; j <= processedCars.rear; j++){
            printf("%d. %s\n",k,processedCars.array[j]);
            k++;
        }
    }
}

/****************************************************************************************************************
 * reports all produced cars
 *
 * @param reportLine
 * @param finishedCars
 * @param headPointer
 */
void allCarsReport(char *reportLine, ProduceCommandsArray *finishedCars,struct Department *headPointer){
    char tempLine[50];
    int TIME;
    char *token;
    int i;


    printf("Command: %s\n",reportLine);
    strcpy(tempLine,reportLine);
    token = strtok(tempLine," ");
    token = strtok(NULL, " ");
    TIME = atoi(token) + 1;

    for (i = 0; i <= finishedCars->top; i++){
        getOneCarReport(finishedCars->cars[i],TIME,headPointer);
    }
    printf("\n");
}

/****************************************************************************************************************
 * gives one car's report
 *
 * @param reportLine
 * @param finishedCars
 * @param headPointer
 */
void oneCarReport(char* reportLine, ProduceCommandsArray *finishedCars,struct Department *headPointer){
    char tempLine[50];
    int TIME;
    char *token;
    int i;

    printf("Command: %s\n",reportLine);

    strcpy(tempLine,reportLine);
    token = strtok(tempLine," ");
    token = strtok(NULL, " ");
    TIME = atoi(token) + 1;
    token = strtok(NULL, " ");
    token = strtok(NULL, " ");

    for (i = 0; i <= finishedCars->top; i++){
        if (strcmp(finishedCars->cars[i]->carCode,token) == 0){
            getOneCarReport(finishedCars->cars[i],TIME,headPointer);
        }
    }
    printf("\n");
}

void getOneCarReport(Car *car, int TIME,struct Department *headPointer){
    char *rept;
    int reptLen;
    int i;
    double timeCounter;
    int depTime;
    int tempTime;
    double process;
    char percent[2];


    rept = (char*)malloc(100 * sizeof(char));
    rept[0] = '\0';
    strcat(rept,"|Report for \0");
    strcat(rept,car->carModel);
    strcat(rept," \0");
    strcat(rept,car->carCode);
    strcat(rept,"|\0");

    reptLen = strlen(rept);
    printNTimesThis("-",reptLen);
    printf("%s\n",rept);
    printNTimesThis("-",reptLen);

    timeCounter = 0;
    for (i = 0; i <= car->carInDepartmentsArray.top; i++){
        printf("%s:",car->carInDepartmentsArray.carInDepartment[i]->departmentName);
        depTime = car->carInDepartmentsArray.carInDepartment[i]->beginTime + car->carInDepartmentsArray.carInDepartment[i]->departmentTime;

        if (depTime <= TIME){
            printf("%d, ",car->carInDepartmentsArray.carInDepartment[i]->departmentTime);
            timeCounter += (double)car->carInDepartmentsArray.carInDepartment[i]->departmentTime;
        }
        else {
            tempTime = depTime - TIME;
            if (tempTime <= car->carInDepartmentsArray.carInDepartment[i]->departmentTime){
                tempTime = car->carInDepartmentsArray.carInDepartment[i]->departmentTime - tempTime;
                timeCounter += (double)tempTime;
                printf("%d, ",tempTime);
            }
            else {
                printf("%d, ",0);
            }
        }
    }
    process = (timeCounter/getDepartmentTotalTime(headPointer))*100;
    percent[0] = '%';
    percent[1] = '\0';
    if (process == 100){
        printf("| Start Time: %d | Complete:%d%s | ",car->startTime,100,percent);
    }
    else {
        printf("| Start Time: %d | Complete:%.4g%s | ", car->startTime, process, percent);
    }
    if (process == 100){
        printf("Complete");
    } else {
        printf("Not complete");
    }
    printf("\n");

}
/************************************************     end of reports   *******************************************************/







/***************************************    UTIL FUNCTIONS   ********************************************/
/**
 * prints given string N times
 * @param value
 * @param N
 */
void printNTimesThis(char* value, int N){
    int i;
    for (i = 0; i < N; i++){
        printf("%s",value);
    }
    printf("\n");
}

/****************************************************************************************************************
 * initialize a Department struct and returns it.
 * @return
 */
struct Department* getNewDepartment(){
    struct Department *department;
    department = (struct Department*)malloc(sizeof(struct Department));
    department->next = NULL;
    department->prev = NULL;
    department->child = NULL;
    department->parent = NULL;
    department->isEmpty = 1;
    department->carEnterTime = -1;
    initializeCarsArray(&department->buildingCar,10);
    initializeBuildedCarsArray(&department->buildedCarsArray,10);
    return department;
}

/****************************************************************************************************************
 * explores departments linked list and keeps proceed time
 *
 * @param headPointer
 * @return
 */
double getDepartmentTotalTime(struct Department *headPointer){
    double count;
    struct Department *pointer;

    count = 0;
    pointer = headPointer;
    if (pointer->next == NULL){
        return count;
    }
    else {
        pointer = pointer->next;
        while (pointer != NULL){
            count += (double)pointer->processTime;
            pointer = pointer->next;
        }
    }
    return count;

}


void initializeCarInDepartmentsArray(CarInDepartmentsArray *carInDepartmentsArray, int initialSize){
    carInDepartmentsArray->carInDepartment = (CarInDepartment**)malloc(initialSize * sizeof(CarInDepartment*));
    carInDepartmentsArray->size = initialSize;
    carInDepartmentsArray->top = -1;
}

void addCarInDeptArray(CarInDepartmentsArray *carInDepartmentsArray, CarInDepartment *carInDepartment){
    if (carInDepartmentsArray->top == carInDepartmentsArray->size - 1){
        carInDepartmentsArray->size *= 2;
        carInDepartmentsArray->carInDepartment = (CarInDepartment**)realloc(carInDepartmentsArray->carInDepartment,carInDepartmentsArray->size * sizeof(CarInDepartment*));
    }
    carInDepartmentsArray->top++;
    carInDepartmentsArray->carInDepartment[carInDepartmentsArray->top] = carInDepartment;
}

void initializeBuildedCarsArray(BuildedCarsArray *buildedCarsArray, int initialSize){
    buildedCarsArray->buildedCars = (BuildedCars**)malloc(initialSize * sizeof(BuildedCars*));
    buildedCarsArray->size = initialSize;
    buildedCarsArray->top = -1;
}

void addBuildedCarsArray(BuildedCarsArray *buildedCarsArray, BuildedCars *newBuildedCar){
    if (buildedCarsArray->top == buildedCarsArray->size - 1){
        buildedCarsArray->size *= 2;
        buildedCarsArray->buildedCars = (BuildedCars**)realloc(buildedCarsArray->buildedCars,buildedCarsArray->size * sizeof(BuildedCars*));
    }
    buildedCarsArray->top++;
    buildedCarsArray->buildedCars[buildedCarsArray->top] = newBuildedCar;
}

void initializeCarsArray(ProduceCommandsArray *produceCommandsArray, int initialSize){
    produceCommandsArray->cars = (Car**)malloc(initialSize * sizeof(Car*));
    produceCommandsArray->size = initialSize;
    produceCommandsArray->top = -1;
}

void initializeDepartmentsArray(AddDeptCommandsArray *addDeptCommandsArray, int initialSize){
    addDeptCommandsArray->departments = (struct Department**)malloc(initialSize * sizeof(struct Department*));
    addDeptCommandsArray->size = initialSize;
    addDeptCommandsArray->top = -1;
}

void addCarsArray(ProduceCommandsArray *produceCommandsArray, Car *newCar){
    if (produceCommandsArray->top == produceCommandsArray->size - 1){
        produceCommandsArray->size *= 2;
        produceCommandsArray->cars = (Car**)realloc(produceCommandsArray->cars,produceCommandsArray->size * sizeof(Car*));
    }
    produceCommandsArray->top++;
    produceCommandsArray->cars[produceCommandsArray->top] = newCar;
}

void addDepartmentArray(AddDeptCommandsArray *addDeptCommandsArray, struct Department *department){
    if (addDeptCommandsArray->top == addDeptCommandsArray->size - 1){
        addDeptCommandsArray->size *= 2;
        addDeptCommandsArray->departments = (struct Department**)realloc(addDeptCommandsArray->departments,addDeptCommandsArray->size * sizeof(struct Department*));
    }
    addDeptCommandsArray->top++;
    addDeptCommandsArray->departments[addDeptCommandsArray->top] = department;
}


/****************************************************************************************************************
 * Queue initializer.
 * Programmer gives a initial size, while adding elements array size increased.
 *
 * @param queue
 * @param initialSize
 */
void initializeQueue(Queue *queue, int initialSize) {
    queue->array = (char**)malloc(initialSize * sizeof(char*));
    queue->front = -1;
    queue->rear = -1;
    queue->size = initialSize;
}

/****************************************************************************************************************
 * Adds queue array one element.
 * Array size will increase itself.
 *
 * @param queue
 * @param element
 */
void enqueue(Queue *queue, char* element){
    if (queue->rear == queue->size -1){
        queue->size *= 2;
        queue->array = (char**)realloc(queue->array, queue->size * sizeof(char*));
    }
    if (queue->front == -1){
        queue->front = 0;
    }
    queue->rear++;
    queue->array[queue->rear] = element;
}

/****************************************************************************************************************
 * returns true if queue is empty. else false
 *
 * @param queue
 * @return
 */
int isEmptyQueue(Queue *queue){
    if (queue->front == -1 || queue->front > queue->rear){
        return 1;
    }
    else {
        return 0;
    }
}


/******************************************       ENF OF UTIL FUNCTIONS     **********************************************/